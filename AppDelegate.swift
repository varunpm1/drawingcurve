//
//  AppDelegate.swift
//  TheBouqs
//
//  Created by Sanjib Chakraborty on 11/02/16.
//  Copyright © 2016 Ymedialabs. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

private typealias DefaultAPICalls = AppDelegate
private typealias CoreDataHandler = AppDelegate

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    var revealController : SWRevealViewController?
    
    var networkReachabilityStatus : AFNetworkReachabilityStatus?
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        let screenBounds:CGRect = UIScreen.mainScreen().bounds
        self.window = UIWindow(frame: screenBounds)
        
        // Set up SDWebImageCache
        setupForImageCache()
        
        // Start reachability
        startReachability()
        
        // Delete local subscription object
        deleteSubscription()
        
        // Fetch all the necessary default data from API
        getBouqStylesFromApi()
        getDeliveryOptionsFromApi()
        getRelationshipTypesFromApi()
        getOccasionTypesFromApi()
        
        // Init root VC
        setupRootVC()
        
        // Init analytics and Fabric
        //- Google Analytics----//
        GAI.sharedInstance().trackUncaughtExceptions = true
        GAI.sharedInstance().logger.logLevel = GAILogLevel.None
        GAI.sharedInstance().dispatchInterval = 120;
        GAI.sharedInstance().trackerWithTrackingId("UA-39651523-44")
        
        #if DEBUG
            GAI.sharedInstance().dryRun = true
        #else
            GAI.sharedInstance().dryRun = false
        #endif
        
        Fabric.with([Crashlytics.self])
        
        self.window?.backgroundColor = UIColor.ColorConstants.White.color()
        self.window?.makeKeyAndVisible()
        
        return true
    }
    
    //MARK: Private helper functions
    // Set up root VC
    private func setupRootVC() {
        var frontNC: UINavigationController = UINavigationController()
        
        // If loading for first time, then fetch all categories first. Else go directly to homeVC
        if (Datamanager.sharedInstance.getAllCategoriesCount() > 0) {
            let homeVC = HomeVC(nibName: getClassName(HomeVC), bundle: nil)
            frontNC = UINavigationController(rootViewController: homeVC)
            frontNC.navigationBar.hidden = true
        }
        else {
            let loadingVC = LoadingVC(nibName: getClassName(LoadingVC), bundle: nil)
            frontNC = UINavigationController(rootViewController: loadingVC)
            frontNC.navigationBar.hidden = true
        }
        
        let rearVC = MenuVC(nibName : self.getClassName(MenuVC), bundle: nil)
        let rearNC = UINavigationController(rootViewController: rearVC)
        rearNC.navigationBarHidden = true
        
        revealController = SWRevealViewController(rearViewController: rearNC, frontViewController: frontNC)
        revealController!.rearViewRevealWidth = 2 * (UIScreen.mainScreen().bounds.size.width / 3)
        revealController!.rearViewRevealDisplacement = UIScreen.mainScreen().bounds.size.width
        revealController!.rearViewRevealOverdraw = 0
        revealController!.frontViewShadowOffset = CGSizeMake(0, 1.0);
        revealController!.frontViewShadowOpacity = 0.3;
        revealController!.toggleAnimationDuration = 0.6
        revealController!.tapGestureRecognizer()
        
        self.window!.rootViewController = revealController!
    }
    
    // Set up image cache
    private func setupForImageCache() {
        SDWebImageManager.sharedManager().delegate = ImageLoader.sharedImageLoader()
        SDImageCache.sharedImageCache().shouldDecompressImages = false
        SDWebImageDownloader.sharedDownloader().shouldDecompressImages = false
    }
    
    // Set up network reachability class
    private func startReachability() {
        AFNetworkReachabilityManager.sharedManager().startMonitoring()
        networkReachabilityStatus = AFNetworkReachabilityManager.sharedManager().networkReachabilityStatus
        AFNetworkReachabilityManager.sharedManager().setReachabilityStatusChangeBlock { [weak self] (status: AFNetworkReachabilityStatus) -> Void in
            if self == nil {
                return
            }
            self!.networkReachabilityStatus = status
            NSNotificationCenter.defaultCenter().postNotificationName(String.LocalNotificationName.NetworkChanged.name(), object: nil)
        }
    }
    
    //MARK: AppDelegate functions
    func applicationDidReceiveMemoryWarning(application: UIApplication) {
        print("AppDelegate Memory Warning Gets Called")
        
        // Flush image data
        SDImageCache.sharedImageCache().clearMemory()
        SDImageCache.sharedImageCache().cleanDisk()
        SDImageCache.sharedImageCache().clearDisk()
        SDImageCache.sharedImageCache().setValue(nil, forKey: "memCache")
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }
}

extension DefaultAPICalls {
    //MARK: Get Default data from backend
    func getBouqStylesFromApi() {
        let bouqStyleApi = BouqStyleAPI()
        bouqStyleApi.callApiToGetBouqStyles(nil)
    }
    
    func getDeliveryOptionsFromApi() {
        let deliveryOptionApi = DeliveryOptionAPI()
        deliveryOptionApi.callApiToGetDeliveryOptions(nil)
    }
    
    func getRelationshipTypesFromApi() {
        let relationshipTypeApi = RelationshipTypeAPI()
        relationshipTypeApi.callApiToGetRelationshipTypes(nil)
    }
    
    func getOccasionTypesFromApi() {
        let occasionApi = OccasionAPI()
        occasionApi.callApiToGetAllOccasionsType(nil)
    }
}


extension CoreDataHandler {
    //MARK: Delete core data objects
    
    // Delete user data
    func deleteUserData() {
        let context = Datamanager.sharedInstance.createWriteContext
        
        deleteCoreDataForClass(getClassName(Relationship), withContext: context)
        deleteCoreDataForClass(getClassName(Address), withContext: context)
        deleteCoreDataForClass(getClassName(Cart), withContext: context)
        deleteCoreDataForClass(getClassName(PaymentSource), withContext: context)
        deleteCoreDataForClass(getClassName(Occasion), withContext: context)
        deleteCoreDataForClass(getClassName(Subscription), withContext: context)
        deleteCoreDataForClass(getClassName(ManageSubscription), withContext: context)
        
        Datamanager.sharedInstance.saveAllWithContext(context) { (isSuccess) in
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.3 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) { () -> Void in
                NSNotificationCenter.defaultCenter().postNotificationName(String.LocalNotificationName.SignOut.name(), object: nil)
            }
        }
    }
    
    private func deleteCoreDataForClass(className : String, withContext context : NSManagedObjectContext) {
        let fetchRequest = NSFetchRequest(entityName: className)
        
        var fetchedObjects = [AnyObject]()
        do {
            fetchedObjects = try context.executeFetchRequest(fetchRequest)
        }
        catch {
            
        }
        
        for object in fetchedObjects {
            context.deleteObject(object as! NSManagedObject)
        }
    }
}

